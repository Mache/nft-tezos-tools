import { assign_metadata } from "./assign_metadata";
import { assets_folder_name, metadata_folder_name, t_phase } from "./config";
import { upload_folder } from "./uploader";
const reveal = async (
  { first_token_id, last_token_id }: t_phase,
  distribution_starting_from: number
) => {
  try {
    console.log("UPLOADING ASSETS");
    const assets = await upload_folder(assets_folder_name, {
      first_token_id,
      last_token_id,
    });
    if (assets.cid) {
      console.log("CREATING METADATA");
      await assign_metadata(
        { first_token_id, last_token_id },
        distribution_starting_from,
        assets.cid
      );
      console.log("UPLOADING METADATA");
      const metadata = await upload_folder(metadata_folder_name, {
        first_token_id,
        last_token_id,
      });
      console.log(`REVEAL USING: ${metadata.cid}`);
      console.log("DONE");
    }
  } catch (error) {
    console.log(error);
  }
};

reveal({ first_token_id: 144, last_token_id: 158 }, 156);
