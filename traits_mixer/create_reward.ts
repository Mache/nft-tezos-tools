import {
  BigMapAbstraction,
  MichelCodecPacker,
  MichelsonMap,
  TezosToolkit,
} from "@taquito/taquito";
import { List, Set } from "immutable";
//import { Network, NetworkType } from "@airgap/beacon-sdk";
//import { BeaconWallet } from "@taquito/beacon-wallet";
//import { castToBigNumber } from "@taquito/rpc";
import Big from "big.js";
import axios from "axios";
export interface t_phase {
  name: string;
  utz_cost: Big;
  from_date: Big;
  to_date: Big;
  from_token_id: Big;
  to_token_id: Big;
  white_listed_only: boolean;
  max_tokens_per_white_listed_wallet: Big;
  max_mint_per_tx: Big;
  last_token_block_level: null | Big;
  minted_tokens: null | Big;
  distribution_starting_from: null | Big;
}
export interface t_reward {
  wanted: { token_id: Big; level: Big }[];
  bonuz: { token_id: Big; level: Big }[];
  expire_on: Big;
  reward_ratio: Big;
  bonuz_jailed: Big[];
  multiplied: Big;
  cashed: Big;
  pool: Big;
  complete: boolean;
  created_with_block_level: Big;
  fugitives_hash: string;
  created: Big;
}
export interface t_sale_contract_storage {
  collection_max_tokens: Big;
  next_token_id: Big;
  next_coin_id: Big;
  next_phase_id: Big;
  provenance_hash: string;
  managers: string[];
  fa2_contract: string;
  hidden_folder_hash: string;
  white_list: MichelsonMap<string, Big[]>;
  last_token_block_level: Big;
  allowed_coin: MichelsonMap<Big, Big>;
  last_phase: null | t_phase;
  phases_history: MichelsonMap<Big, t_phase>;
  rewards_mint_share: Big;
  rewards_list: MichelsonMap<Big, t_reward>;
  next_reward_id: Big;
  jailed: MichelsonMap<Big, null>;
  eaten: MichelsonMap<Big, null>;
  ledger: BigMapAbstraction;
  metadata: BigMapAbstraction;
  supply: BigMapAbstraction;
  token_metadata: BigMapAbstraction;
}
export interface t_level {
  level: string;
  exp: Big;
}
export interface t_fa2_contract_storage {
  administrator: string;
  exp_chart: MichelsonMap<Big, t_level>;
  fa2_manager: string;
  last_token_id: Big;
  last_token_id_revealed: Big;
  ledger: BigMapAbstraction;
  metadata: BigMapAbstraction;
  operators: BigMapAbstraction;
  token_metadata: BigMapAbstraction;
}
const SALE_CONTRACT_ADDRESS = "KT1W8Q1facYYeiwMLh1JVEGE9YZcsUXwXrk3";
const FA2_CONTRACT_ADDRESS = "KT1D71degk4dMLtAWdpWgk8Ycg1tfeNdBNHz";
export type _error = { data: { with: { string: string } }[] };
//const network: Network = { type: NetworkType.HANGZHOUNET };
//const RPC_TEST_NODE = "https://ithacanet.smartpy.io";
const RPC_MAIN_NODE = "https://mainnet-tezos.giganode.io";
const Tezos = new TezosToolkit(RPC_MAIN_NODE);
Tezos.setPackerProvider(new MichelCodecPacker());
/*const wallet = new BeaconWallet({
  name: "FINGERZ TZ - XPENDING MACHINE",
  preferredNetwork: network.type,
});
Tezos.setWalletProvider(wallet);*/
export const get_fa2_contract_storage = async (tezos: TezosToolkit) => {
  let fa2_storage: t_fa2_contract_storage | null = null;
  try {
    console.log("Getting FA2 contract storage");
    //const contract = await tezos.wallet.at(FA2_CONTRACT_ADDRESS);
    //console.log("Waiting operation finish");
    //const result = await contract.storage<t_fa2_contract_storage>();
    fa2_storage = await tezos.contract.getStorage<t_fa2_contract_storage>(
      FA2_CONTRACT_ADDRESS
    );
    console.log("FA2 contract storage gotten ");
    return fa2_storage;
  } catch (error) {
    console.log(error);
    const my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return fa2_storage;
  }
};
export const get_sale_contract_storage = async (tezos: TezosToolkit) => {
  let sale_storage: t_sale_contract_storage | null = null;
  try {
    console.log("Getting SALEZ contract storage");
    //const contract = await tezos.wallet.at(SALE_CONTRACT_ADDRESS);

    //console.log("Waiting operation finish");
    //const result = await contract.storage<t_sale_contract_storage>();
    sale_storage = await tezos.contract.getStorage<t_sale_contract_storage>(
      SALE_CONTRACT_ADDRESS
    );
    console.log("SALEZ contract storage gotten ");
    return sale_storage;
  } catch (error) {
    console.log(error);
    const my_error: _error = error as _error;
    console.log(my_error.data[1]?.with.string);
    return sale_storage;
  }
};

const create_reward = async (
  phases_to_get: number[],
  random_indexes: number[],
  banned_dev_tokens: number[]
) => {
  const fa2_storage: t_fa2_contract_storage | null =
    await get_fa2_contract_storage(Tezos);
  const sale_storage: t_sale_contract_storage | null =
    await get_sale_contract_storage(Tezos);
  //console.log(fa2_storage);
  //console.log(sale_storage);
  const query_time = await axios.get<{ unixtime: number }>(
    "https://worldtimeapi.org/api/timezone/America/Argentina/Salta"
  );
  const { unixtime } = query_time.data;
  let on_wanted: Set<number> = Set();
  let on_jailed: Set<number> = Set();
  let on_eaten: Set<number> = Set();
  if (sale_storage && fa2_storage) {
    if (sale_storage.jailed) {
      for (const jailed_id of sale_storage.jailed.keys()) {
        on_jailed = on_jailed.add(jailed_id.toNumber());
        //on_jailed = on_jailed.add(Number.parseInt(jailed_id.toFixed(), 10));
      }
    }
    if (sale_storage.eaten) {
      for (const eaten_id of sale_storage.eaten.keys()) {
        on_eaten = on_eaten.add(eaten_id.toNumber());
        //on_jailed = on_jailed.add(Number.parseInt(jailed_id.toFixed(), 10));
      }
    }
    for (const value of sale_storage.rewards_list.values()) {
      const expiration = value.expire_on.toNumber(); // Number.parseInt(value.expire_on.toFixed(), 10);
      //console.log("BIG NUMBER TEST", expiration === value.expire_on.toNumber());
      const current_time = unixtime;
      const not_expired = expiration > current_time;
      if (~value.complete && not_expired) {
        for (const { token_id } of value.wanted) {
          on_wanted = on_wanted.add(token_id.toNumber());
          //on_wanted = on_wanted.add(Number.parseInt(token_id.toFixed(), 10));
        }
        for (const { token_id } of value.bonuz) {
          on_wanted = on_wanted.add(token_id.toNumber());
          //on_wanted = on_wanted.add(Number.parseInt(token_id.toFixed(), 10));
        }
      }
    }
    const on_banned = on_jailed
      .union(on_wanted)
      .union(on_eaten)
      .union(banned_dev_tokens);
    const banned: Set<number> = on_banned;
    console.log(
      "JAILED: ",
      on_jailed.toArray().sort((a, b) => a - b)
    );
    console.log(
      "EATEN: ",
      on_eaten.toArray().sort((a, b) => a - b)
    );
    console.log(
      "WANTED: ",
      on_wanted.toArray().sort((a, b) => a - b)
    );
    console.log(
      "BANNED DEV TOKENS: ",
      banned_dev_tokens.sort((a, b) => a - b)
    );
    console.log(
      "BANNED (JAILED+WANTED+EATEN+DEV): ",
      on_banned.toArray().sort((a, b) => a - b)
    );

    let phases_gotten: List<t_phase> = List();
    for (const phase_id of phases_to_get) {
      //const casted_phase_id = castToBigNumber([phase_id]) as number[];
      const phase_id_zero = Big(phase_id); //casted_phase_id[0];
      const exist_phase = sale_storage.phases_history.has(phase_id_zero);

      if (exist_phase) {
        const phase = sale_storage.phases_history.get(phase_id_zero) as t_phase;
        phases_gotten = phases_gotten.push(phase);
      }
    }
    const phases: List<t_phase> = phases_gotten;
    console.log("PHASES LISTED ", phases.toArray());
    let from_token_id = 0;
    let to_token_id = sale_storage.next_token_id.toNumber(); //Number.parseInt(sale_storage.next_token_id.toFixed(), 10);
    let ledger_keys: Set<number> = Set(); // Set(filler).subtract(banned.toArray());
    if (phases_gotten.size > 0) {
      if (phases_gotten.size > 1) {
        const first_phase = phases_gotten.first();
        const last_phase = phases_gotten.last();
        if (first_phase && last_phase) {
          from_token_id = first_phase.from_token_id.toNumber(); //Number.parseInt(last_phase.from_token_id.toFixed(), 10);
          to_token_id = last_phase.to_token_id.toNumber(); // Number.parseInt(last_phase.to_token_id.toFixed(), 10) + 1;
        }
      } else {
        const first_phase = phases_gotten.first();
        if (first_phase) {
          from_token_id = first_phase.from_token_id.toNumber(); //Number.parseInt(last_phase.from_token_id.toFixed(), 10);
          to_token_id = first_phase.to_token_id.toNumber(); // Number.parseInt(last_phase.to_token_id.toFixed(), 10) + 1;
        }
      }

      console.log("FROM: ", from_token_id);
      console.log("TO: ", to_token_id);
      to_token_id += 1;
      const filler = Array(to_token_id)
        .fill(null, from_token_id, to_token_id)
        .map((_, index) => index)
        .filter((element) => element != null)
        .sort((a, b) => a - b);
      console.log("SUPPLY: ", filler.length);
      console.log("FILLER: ", filler);
      ledger_keys = ledger_keys.union(Set(filler));
    }
    ledger_keys = ledger_keys.subtract(banned.toArray());
    console.log(
      "LEDGER KEYS: ",
      ledger_keys.toArray().sort((a, b) => a - b)
    );
    for (const index of random_indexes) {
      const wanted = from_token_id + index;
      const is_white_liested = ledger_keys.toArray().includes(wanted);
      if (is_white_liested) {
        console.log(
          `RANDOM INDEX #${index} + FROM TOKEN ID #${from_token_id} = #${wanted} [IS WHITE LISTED]`
        );
      } else {
        console.log(`REROLL`);
        console.log(
          `RANDOM INDEX #${index}: WANTED #${wanted} IS INCLUDED ON BANNED WALLETS (DEV): ${banned_dev_tokens.includes(
            wanted
          )}`
        );
        console.log(
          `RANDOM INDEX #${index}: WANTED #${wanted} IS INCLUDED ON JAILED FINGERZ (COMPLETED MISSIONS): ${on_jailed
            .toArray()
            .includes(wanted)}`
        );
        console.log(
          `RANDOM INDEX #${index}: WANTED #${wanted} IS INCLUDED ON LOST IN MINES (GOLD FEVER): ${on_eaten
            .toArray()
            .includes(wanted)}`
        );
        console.log(
          `RANDOM INDEX #${index}: WANTED #${wanted} IS INCLUDED ON ACTIVE WANTED MISSION: ${on_wanted
            .toArray()
            .includes(wanted)}`
        );
        console.log(
          `RANDOM INDEX #${index}: WANTED #${wanted} IS OUT OF RANGE: ${
            ledger_keys.toArray().length < wanted
          }`
        );
      }
    }
    /*let wanted: List<{ token_id: number; level: number }> = List();
    const fingerzens = ledger_keys.toArray();
    for (let level = 1; level <= 5; level++) {
      console.log(fingerzens);
      const my_random = Math.floor(Math.random() * fingerzens.length);
      console.log(my_random);
      const token_id = fingerzens[my_random];
      console.log(token_id);
      if (token_id !== undefined) {
        wanted = wanted.push({
          token_id,
          level: 0,
        });
        fingerzens.splice(my_random, 1);
        console.log(fingerzens);
      }
    }
    let bonuz: List<{ token_id: number; level: number }> = List();
    for (let level = 1; level <= 5; level++) {
      const my_random = Math.floor(Math.random() * fingerzens.length);
      console.log(my_random);
      const token_id = fingerzens[my_random];
      console.log(token_id);
      if (token_id !== undefined) {
        bonuz = bonuz.push({
          token_id,
          level,
        });
        fingerzens.splice(my_random, 1);
        console.log(fingerzens);
      }
    }
    console.log("WANTED: ", wanted.toJSON());
    console.log("BONUZ: ", bonuz.toJSON());
    */
    /*let fingerzens = Set<number>();
    const nft_list = await fa2_storage.ledger.getMultipleValues<string>(
      ledger_keys.toArray()
    );
    for (const [key, value] of nft_list.entries()) {
      if (value) {
        //on_infractors = on_infractors.add(key.toNumber());
        fingerzens = fingerzens.add(Number.parseInt(key.toString(), 10));
      }
    }

    console.log(
      "Fingerzens: ",
      fingerzens.toArray().sort((a, b) => a - b)
    );*/
    //console.log(fa2_storage.ledger)
  }
};
create_reward([0, 1], [95], [0, 1, 87, 22, 86]);
