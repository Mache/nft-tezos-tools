import path from "path";
import fs from "fs";
import sha256_hash from "./sha256_hash";
import { assets_folder_path } from "./config";
import { load_collection } from "./load_collection";

export const verify_assets_hash = async () => {
  const files_list = await fs.promises.readdir(assets_folder_path);
  const collection_file_path = path.join(__dirname, "collection.json");
  const collection = await load_collection(collection_file_path);
  files_list.forEach(async (file_name) => {
    const file_path = path.join(assets_folder_path, file_name);
    const file_buffer = await fs.promises.readFile(file_path);
    const file_hash = await sha256_hash(file_buffer);
    const file_name_splited = file_name.split(".");
    const file_index = Number.parseInt(file_name_splited[0], 10);
    const file_level = Number.parseInt(file_name_splited[2], 10);
    if (file_level === 0) {
      const artifact_hash = collection
        .get(file_index)
        ?.levels.get(0)?.artifact_hash;
      if (artifact_hash) {
        console.log(
          `checking: [${file_index}] ${file_name}: ${file_hash} === ${artifact_hash}`
        );
        if (artifact_hash !== file_hash) {
          throw new Error("ARTIFACT HASH NOT THE SAME AS FILE HASH");
        }
      } else {
        throw new Error("ARTIFACT HASH NOT FOUND");
      }
    }
  });
};
export const verify_assets_indexed_hash = async () => {
  const files_list = await fs.promises.readdir(assets_folder_path);
  const collection_file_path = path.join(__dirname, "collection.json");
  const collection = await load_collection(collection_file_path);
  files_list.forEach(async (file_name) => {
    const file_path = path.join(assets_folder_path, file_name);
    const file_buffer = await fs.promises.readFile(file_path);
    const file_hash = await sha256_hash(file_buffer);
    const file_name_splited = file_name.split(".");
    const file_index = Number.parseInt(file_name_splited[0], 10);
    const file_level = Number.parseInt(file_name_splited[2], 10);
    const file_indexed_hash = await sha256_hash(
      Buffer.from(`${file_index}@${file_hash}`)
    );
    if (file_level === 0) {
      const artifact_hash = collection
        .get(file_index)
        ?.levels.get(0)?.indexed_hash;
      if (artifact_hash) {
        console.log(
          `checking: [${file_index}] ${file_name}: ${file_indexed_hash} === ${artifact_hash}`
        );
        if (artifact_hash !== file_indexed_hash) {
          throw new Error("ARTIFACT HASH NOT THE SAME AS FILE HASH");
        }
      } else {
        throw new Error("ARTIFACT HASH NOT FOUND");
      }
    }
  });
};
export const verify_provenance_hash = async (provenance_hash: string) => {
  const files_list = await fs.promises.readdir(assets_folder_path);
  //const collection_file_path = path.join(__dirname, "collection.json");
  //const collection = await load_collection(collection_file_path);
  const hashes_from_files = await Promise.all(
    files_list
      .filter((file_name) => {
        const file_name_splited = file_name.split(".");
        const file_level = Number.parseInt(file_name_splited[2], 10);
        return file_level === 0;
      })
      .sort((file_a, file_b) => {
        const file_a_splitted = file_a.split(".");
        const file_a_index = Number.parseInt(file_a_splitted[0], 10);
        const file_b_splitted = file_b.split(".");
        const file_b_index = Number.parseInt(file_b_splitted[0], 10);
        return file_a_index - file_b_index;
      })
      .map(async (file_name) => {
        //console.log(file_name);
        const file_path = path.join(assets_folder_path, file_name);
        const file_buffer = await fs.promises.readFile(file_path);
        const file_hash = await sha256_hash(file_buffer);
        return file_hash;
      })
  );
  const hash = hashes_from_files.reduce((prev, currrent, index) => {
    if (currrent) {
      return (prev += currrent);
    } else {
      throw new Error(`HASH NOT FOUND [${index}] ${currrent}`);
    }
  }, "");
  const buffered_hash = Buffer.from(hash);
  const provenance_from_files = await sha256_hash(buffered_hash);
  console.log(
    `[${
      provenance_from_files === provenance_hash
    }] ${provenance_from_files} === ${provenance_hash}`
  );
};
verify_assets_indexed_hash();
/*verify_provenance_hash(
  "e419f1e168284060c38363db4a90201696745c39ebe7b96a286fabe478825f95"
);*/
//verify_assets_hash();
