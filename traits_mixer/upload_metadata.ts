import { metadata_folder_name } from "./config";
import { upload_folder } from "./uploader";

upload_folder(metadata_folder_name, {
  first_token_id: 144,
  last_token_id: 158,
});
