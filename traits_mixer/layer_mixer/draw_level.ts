import sharp from "sharp";
import { width, height } from "../config";
import { write_text } from "./write_text";

export const level_trait = async (
  my_traits: sharp.OverlayOptions[],
  level: number
) => {
  const my_color_level = [
    "#000000", //0
    "#FFFFFF", //1
    "#808080", //2
    "#008000", //3
    "#4682B4", //4
    "#BA55D3", //5
    "#CCCC00", //6
    "#8B0000", //7
    "#FFA500", //8
    "#FFD700", //9
    "#FF4500", //10
  ];
  const level_layer = write_text(
    `${level}`,
    width,
    height,
    my_color_level[level]
  );

  return [...my_traits, { input: level_layer, top: 0, left: 0 }];
};
