import { List } from "immutable";
import sharp from "sharp";
import { t_attributes, t_overlay_options } from "../config";
interface t_my_traits_map {
  [level: string]: number; //layer (trait)
}

const my_traits_maps: t_my_traits_map = {
  1: 0,
  2: 1,
  3: 2,
  4: 3,
  5: 4,
  6: 7,
  7: 8,
};
const golden_trait = async (
  attributes: t_attributes,
  layers: t_overlay_options,
  level: number
): Promise<{
  golden_attributes: t_attributes;
  golden_layers: t_overlay_options;
}> => {
  let my_golden_layers: t_overlay_options = List();
  let my_golden_attribute: t_attributes = List(attributes);
  const my_level = my_traits_maps[level];
  const my_layer = layers.get(my_level);
  if (my_layer) {
    const my_buffer_input = my_layer.input as Buffer;
    const my_tinted = await sharp(my_buffer_input)
      .tint({
        r: Math.floor(212),
        g: Math.floor(175),
        b: Math.floor(55),
      })
      .toBuffer();
    my_golden_layers = layers.set(my_level, { input: my_tinted });
  }

  const my_attribute = my_golden_attribute.get(my_level);
  if (my_attribute) {
    my_golden_attribute = my_golden_attribute.update(
      my_level,
      my_attribute,
      (attribute) => {
        return { ...attribute, value: `golden ${attribute?.value}` };
      }
    );
  }

  return {
    golden_attributes: my_golden_attribute,
    golden_layers: my_golden_layers,
  };
};
export default golden_trait;
