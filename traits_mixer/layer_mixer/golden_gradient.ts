import sharp from "sharp";
import { max_level } from "../config";

export const golden_back = async (
  my_traits: sharp.OverlayOptions[],
  level: number
) => {
  const my_back = my_traits.shift();
  if (my_back) {
    const my_input = my_back.input as string;
    const my_tinted = await sharp(my_input)
      .tint({
        r: Math.floor(212 * (level / max_level)),
        g: Math.floor(175 * (level / max_level)),
        b: Math.floor(55 * (level / max_level)),
      })
      .toBuffer();
    return [{ input: my_tinted }, ...my_traits];
  }
  return my_traits;
};
