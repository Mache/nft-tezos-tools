import { width, height } from "../config";

export const write_text = (
  text: string,
  top: number,
  left: number,
  stroke = "#000000",
  fontSize = 150,
  fontVariant = "bold",
  color = "rgba(0,0,0,0.9)",
  fontFamily = "sans-serif"
) => {
  const svgText = `
    <svg viewBox="0 0 ${width} ${height}" xmlns="http://www.w3.org/2000/svg">
  <style>
    .my_text { font: ${fontVariant} ${fontSize}px ${fontFamily}; fill: ${color}; }
  </style>
  <text x="${left - 150}" y="${top - 75}" class="my_text">${text}</text>
  <circle cx="${left - 98}" cy="${top - 130}" r="${
    fontSize / 2
  }" stroke="black" fill="transparent" stroke-width="20"/>
  <rect x="${0}" y="${0}" width="${width}" height="${height}"  stroke="${stroke}" fill="transparent" stroke-width="30"/>
</svg>`;
  return Buffer.from(svgText);
  //
};
