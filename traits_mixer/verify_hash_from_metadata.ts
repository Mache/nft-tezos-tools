import path from "path";
import fs from "fs";
import sha256_hash from "./sha256_hash";
import {
  assets_folder_path,
  max_level,
  metadata_folder_path,
  min_level,
  t_metadata,
} from "./config";

export const verify_hash_from_metadata = async (
  token_id: number,
  level: number
) => {
  const metadata_file_path = path.join(
    metadata_folder_path,
    `${token_id}.level.${level}.json`
  );

  const metadata_buffer = await fs.promises.readFile(metadata_file_path);
  const metadata_file = metadata_buffer.toString();
  const metadata = JSON.parse(metadata_file) as t_metadata;
  const image_file_name = metadata.artifactUri.split("/").pop() as string;
  //console.log(image_file_name);
  const image_file_path = path.join(assets_folder_path, image_file_name);
  const image_index = image_file_name.split(".").shift() as string;
  const artifact_buffer = await fs.promises.readFile(image_file_path);
  const artifact_hash = await sha256_hash(artifact_buffer);

  const indexed_hash = await sha256_hash(
    Buffer.from(`${image_index}@${artifact_hash}`)
  );
  console.log(
    `Token ID: ${token_id} [LEVEL ${level}]\nFile Artifact Hash    : ${artifact_hash}\nMetadata Artifact Hash: ${
      metadata.artifactHash
    }\nVerified: ${
      metadata.artifactHash == artifact_hash
    }\nFile Indexed Hash    : ${indexed_hash}\nMetadata Indexed Hash: ${
      metadata.indexedHash
    }\nVerified: ${metadata.indexedHash == indexed_hash}`
  );
};

const main = (token_id: number) => {
  for (let level = min_level; level <= max_level; level++) {
    verify_hash_from_metadata(token_id, level);
  }
};

main(3);
