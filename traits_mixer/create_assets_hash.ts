import path from "path";

import {
  collection_size,
  max_duplication_factor,
  resume,
  throw_on_max_duplication_factor,
  t_attributes,
  t_collectible,
  t_collection,
  t_overlay_options,
} from "./config";
import { List, Map } from "immutable";
import load_assets, { t_assets } from "./load_assets";
import randomize_layers from "./randomize_layers";
import attributes_md5_hash from "./attributes_md5_hash";
import { load_collection } from "./load_collection";

const attributes_mixer = async (
  collection_index: number,
  assets: t_assets
): Promise<{ collectible: t_collectible; layers: t_overlay_options }> => {
  let my_layers: t_overlay_options = List();
  let my_attributes: t_attributes = List();

  for (const category_index of assets.keySeq()) {
    const category = assets.get(category_index); //as t_category;

    if (category) {
      const { category_name, layers } = category;
      const my_randomized_layers = randomize_layers(layers);
      if (my_randomized_layers) {
        const { layer_name, input } = my_randomized_layers;
        my_layers = my_layers.push({ input });
        my_attributes = my_attributes.push({
          name: category_name,
          value: layer_name,
        });
      }
    }
  }
  const attributes_hash = await attributes_md5_hash(my_attributes);

  return {
    collectible: {
      attributes: my_attributes,
      id: collection_index,
      attributes_hash,
      same_as: [], //duplicated_attributes.keySeq().toArray(),
      levels: Map(),
    },
    layers: my_layers,
  };
};

const create_assets_hash = async (to_index: number) => {
  let collection: t_collection = Map();
  const assets = await load_assets();
  let re_mixed = 0;
  let duplicated = 0;
  const collection_file_path = path.join(__dirname, "collection.json");
  if (resume) {
    collection = await load_collection(collection_file_path);
  }

  let collection_index = collection.size;
  while (collection_index < to_index) {
    console.log("GENERATING ASSEST #", collection_index);
    let mixed = await attributes_mixer(collection_index, assets);
    let duplicated_attributes = collection.findKey(
      ({ attributes_hash }) =>
        attributes_hash == mixed.collectible.attributes_hash
    );

    let duplicated_times = 0;
    while (
      duplicated_attributes != undefined &&
      duplicated_times < max_duplication_factor
    ) {
      duplicated_times += 1;
      re_mixed += 1;
      console.log("MIXING AGAIN - DUPLICATED");
      mixed = await attributes_mixer(collection_index, assets);
      duplicated_attributes = collection.findKey(
        ({ attributes_hash }) =>
          attributes_hash == mixed.collectible.attributes_hash
      );
    }

    if (duplicated_times >= max_duplication_factor) {
      console.log(`MIXED ${max_duplication_factor} TIMES - NO UNIQUE FOUND`);
      duplicated += 1;
      if (throw_on_max_duplication_factor) {
        throw new Error("NEED MORE TRAITS");
      }
    }

    const my_colectible = mixed.collectible;
    collection = collection.set(collection_index, my_colectible);
    collection_index++;
  }
  console.log("RE MIEXED: ", re_mixed);
  console.log("DUPLICATED: ", duplicated);
  //console.log(collection.toJSON());
};

create_assets_hash(collection_size);
