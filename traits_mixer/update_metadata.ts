import fs from "fs";
import path from "path";
import { metadata_folder_path, t_metadata } from "./config";

export const update_metadata = async (
  revealed_folder_hash: string,
  from_id = 0,
  to_id?: number
) => {
  try {
    const all_metadata_files = await fs.promises.readdir(metadata_folder_path);
    const to_end = to_id ? to_id : all_metadata_files.length - 1;
    for (const metadata_file_name of all_metadata_files) {
      const metadata_file_name_splited = metadata_file_name.split(".");
      const metadata_id = Number.parseInt(metadata_file_name_splited[0], 10);
      if (metadata_id >= from_id && metadata_id <= to_end) {
        const metadata_file_path = path.join(
          metadata_folder_path,
          metadata_file_name
        );
        const metadata_level = metadata_file_name_splited[2];
        const metadata_buffer = await fs.promises.readFile(metadata_file_path);
        const metadata_stringify = metadata_buffer.toString();
        const metadata: t_metadata = JSON.parse(
          metadata_stringify
        ) as t_metadata;
        const { id, formats, attributes } = metadata;
        const asset_uri = `ipfs://${revealed_folder_hash}/${id}.level.${metadata_level}.png`;
        const updated_formats = formats.map((my_format) => {
          return { ...my_format, uri: asset_uri };
        });
        const update_attributes = attributes.map((my_attribute) => {
          return my_attribute.name == "level"
            ? { ...my_attribute, value: `${metadata_level}` }
            : my_attribute;
        });
        metadata.artifactUri = asset_uri;
        metadata.displayUri = asset_uri;
        metadata.thumbnailUri = asset_uri;
        metadata.formats = updated_formats;
        metadata.attributes = update_attributes;
        fs.promises.writeFile(
          metadata_file_path,
          JSON.stringify(metadata),
          "utf8"
        );
      }
    }
    // console.log(file_path)
  } catch (error) {
    console.log(error);
  }
};
