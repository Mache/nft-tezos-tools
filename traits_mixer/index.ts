import path from "path";
import fs from "fs";
import layer_mixer from "./layer_mixer";
import golden_trait from "./layer_mixer/golden_trait";
import {
  assets_folder_path,
  collection_size,
  max_duplication_factor,
  max_level,
  resume,
  throw_on_max_duplication_factor,
  t_attributes,
  t_collectible,
  t_collection,
  t_levels,
  t_overlay_options,
} from "./config";
import { List, Map } from "immutable";
import load_assets, { t_assets } from "./load_assets";
import randomize_layers from "./randomize_layers";
import attributes_md5_hash from "./attributes_md5_hash";

const level_me_up = async (
  collectible: t_collectible,
  layers: t_overlay_options
): Promise<t_collectible> => {
  let my_golden_attributes = collectible.attributes;
  let my_golden_layers = layers;
  const collection_index = collectible.id;
  //const my_mixed_layers = [];
  let my_levels: t_levels = collectible.levels.set(
    0,
    await layer_mixer(
      collection_index,
      my_golden_layers,
      0,
      my_golden_attributes
    )
  );
  /*my_mixed_layers.push(
    layer_mixer(collection_index, my_golden_layers, 0, my_golden_attributes)
  );*/
  for (let level = 1; level <= max_level; level++) {
    const { golden_attributes, golden_layers } = await golden_trait(
      my_golden_attributes,
      my_golden_layers,
      level
    );
    my_golden_attributes = golden_attributes;
    my_golden_layers = golden_layers;
    my_levels = my_levels.set(
      level,
      await layer_mixer(
        collection_index,
        golden_layers,
        level,
        my_golden_attributes
      )
    );
    /*my_mixed_layers.push(
      layer_mixer(collection_index, golden_layers, level, my_golden_attributes)
    );*/
  }
  return {
    ...collectible,
    levels: my_levels,
  };
  /*return Promise.all(my_mixed_layers).then((result) => {
    return result.reduce(
      (prev, { level, attributes, indexed_hash, artifact_hash }) => {
        prev.levels = prev.levels.set(level, {
          attributes,
          indexed_hash,
          artifact_hash,
        });
        return prev;
      },
      collectible
    );
  });*/
};

const get_id_of_duplicated_attributes = (
  collection: t_collection,
  my_hash: string
) => {
  return collection.filter(
    (collectible) => collectible.attributes_hash === my_hash
  );
};
const attributes_mixer = async (
  collection_index: number,
  assets: t_assets,
  collection: t_collection,
  duplication_factor = 0
): Promise<{ collectible: t_collectible; layers: t_overlay_options }> => {
  let my_layers: t_overlay_options = List();
  let my_attributes: t_attributes = List();

  for (const category_index of assets.keySeq()) {
    const category = assets.get(category_index); //as t_category;

    if (category) {
      const { category_name, layers } = category;
      const my_randomized_layers = randomize_layers(layers);
      if (my_randomized_layers) {
        const { layer_name, input } = my_randomized_layers;
        my_layers = my_layers.push({ input });
        my_attributes = my_attributes.push({
          name: category_name,
          value: layer_name,
        });
      }
    }
  }
  //console.log("COLLECTION SIZE: ", collection.size);
  const attributes_hash = await attributes_md5_hash(my_attributes);
  const duplicated_attributes = get_id_of_duplicated_attributes(
    collection,
    attributes_hash
  );
  if (
    duplicated_attributes.size > 0 &&
    duplication_factor < max_duplication_factor
  ) {
    console.log("MIXING AGAIN - DUPLICATED");
    return attributes_mixer(
      collection_index,
      assets,
      collection,
      duplication_factor + 1
    );
  }

  if (duplication_factor >= max_duplication_factor) {
    console.log(`MIXED ${max_duplication_factor} TIMES - NO UNIQUE FOUND`);
    if (throw_on_max_duplication_factor) {
      throw new Error("NEED MORE TRAITS");
    }
  }

  return {
    collectible: {
      attributes: my_attributes,
      id: collection_index,
      attributes_hash,
      same_as: duplicated_attributes.keySeq().toArray(),
      levels: Map(),
    },
    layers: my_layers,
  };
};
const clear_path = async (path_to_clear: string) => {
  try {
    const files = await fs.promises.readdir(path_to_clear);
    await Promise.all(
      files.map((file) => fs.promises.unlink(path.join(path_to_clear, file)))
    );
    console.log(`PATH ${path_to_clear} CLEAR`);
  } catch (error) {
    console.log(error);
  }
};
const mixer = async (to_index: number) => {
  let collection: t_collection = Map();
  const assets = await load_assets();

  const collection_file_path = path.join(__dirname, "collection.json");
  if (resume) {
    const collection_file = await fs.promises.readFile(collection_file_path);
    collection = Map(JSON.parse(collection_file.toString()));
  } else {
    await clear_path(assets_folder_path);
  }
  //const all_traits_categories = await fs.promises.readdir(path_to_categories);
  let collection_index = collection.size;
  //let on_process: List<Promise<t_collectible>> = List();
  while (collection_index < to_index) {
    console.log("GENERATING ASSEST #", collection_index);
    const { collectible, layers } = await attributes_mixer(
      collection_index,
      assets,
      collection
    );
    const my_colectible = await level_me_up(collectible, layers);
    collection = collection.set(collection_index, my_colectible);
    //on_process = on_process.push(level_me_up(collectible, layers));

    collection_index++;
  }
  /*collection = await Promise.all(on_process).then((result) => {
    return result.reduce((prev, value) => {
      return prev.set(value.id, value);
    }, collection);
  });*/
  fs.promises.writeFile(
    collection_file_path,
    JSON.stringify(collection.toJSON())
  );
};

mixer(collection_size);
