import {
  collection_size,
  hidden_token_artifact,
  hidden_token_display,
  hidden_token_thumbnail,
  max_level,
  min_level,
  unknown_metadata_folder_path,
} from "./config";
import create_unknown_metadata from "./create_unknown_metadata";
import fs from "fs";
import path from "path";
const clear_path = async (path_to_clear: string) => {
  try {
    const files = await fs.promises.readdir(path_to_clear);
    await Promise.all(
      files.map((file) => fs.promises.unlink(path.join(path_to_clear, file)))
    );
    console.log(`PATH ${path_to_clear} CLEAR`);
  } catch (error) {
    console.log(error);
  }
};
const assign_unknown_metadata = async (size: number) => {
  try {
    await clear_path(unknown_metadata_folder_path);
    for (let token_id = 0; token_id <= size; token_id++) {
      for (let level = min_level; level <= max_level; level++) {
        create_unknown_metadata(
          unknown_metadata_folder_path,
          hidden_token_artifact,
          hidden_token_display,
          hidden_token_thumbnail,
          token_id,
          level
        );
      }
    }
  } catch (error) {
    console.log(error);
  }
};

assign_unknown_metadata(collection_size);
