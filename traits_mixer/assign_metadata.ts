import path from "path";
import {
  max_level,
  metadata_folder_path,
  min_level,
  t_attributes,
  t_collection,
  t_level,
  t_levels,
  t_phase,
} from "./config";
import { create_metadata } from "./create_metadata";
import fs from "fs";
import { List, Map } from "immutable";

const clear_path = async (path_to_clear: string) => {
  try {
    const files = await fs.promises.readdir(path_to_clear);
    await Promise.all(
      files.map((file) => fs.promises.unlink(path.join(path_to_clear, file)))
    );
    console.log(`PATH ${path_to_clear} CLEAR`);
  } catch (error) {
    console.log(error);
  }
};

export const assign_metadata = async (
  phase: t_phase,
  distribution_starting_from: number,
  fingerz_revealed_assets: string
) => {
  const start_from = distribution_starting_from - phase.first_token_id;
  let collection: t_collection = Map();
  const collection_file_path = path.join(__dirname, "collection.json");
  const collection_file = await fs.promises.readFile(collection_file_path);
  const my_collection = JSON.parse(collection_file.toString());
  let next_collection: t_collection = Map();
  Object.keys(my_collection).forEach((index) => {
    const my_collectible = my_collection[index];
    const my_levels = my_collectible.levels;
    const collection_index = Number.parseInt(index, 10);
    let next_levels: t_levels = Map();
    Object.keys(my_levels).forEach((level) => {
      const my_level = my_levels[level] as t_level;
      const my_attributes: t_attributes = List(my_level.attributes);
      const level_index = Number.parseInt(level, 10);
      next_levels = next_levels.set(level_index, {
        ...my_level,
        attributes: my_attributes,
      });
    });
    next_collection = next_collection.set(collection_index, {
      ...my_collectible,
      levels: next_levels,
    });
  });
  collection = next_collection;
  await clear_path(metadata_folder_path);
  for (
    let token_id = phase.first_token_id;
    token_id <= phase.last_token_id;
    token_id++
  ) {
    const my_asset =
      phase.last_token_id - token_id >= start_from
        ? token_id + start_from
        : phase.first_token_id + phase.last_token_id - token_id;
    const my_collectible = collection.get(my_asset);

    console.log(`GENERATING METADATA FOR TOKEN ID: ${token_id}`);
    if (my_collectible) {
      console.log(`ASSET FOUND ON INDEX: ${my_asset}`);
      for (let level = min_level; level <= max_level; level++) {
        await create_metadata(
          metadata_folder_path,
          fingerz_revealed_assets,
          token_id,
          my_collectible,
          level
        );
      }
    }
  }
};
