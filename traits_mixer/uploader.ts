import { NFTStorage, File, CIDString } from "nft.storage";
import fs from "fs";
import mime from "mime";
import path from "path";
import { t_phase } from "./config";
import { StatusResult } from "nft.storage/dist/src/lib/interface";
import "dotenv/config";

const my_file = new File([], "");
type t_file = typeof my_file;
const api_token = process.env.STORAGE_API_TOKEN as string;

export const upload_folder = async (folder_name: string, phase?: t_phase) => {
  let cid: null | CIDString = null;
  let status: null | StatusResult = null;
  try {
    const folder_path = path.join(__dirname, folder_name);
    const my_files = await fs.promises.readdir(folder_path);
    const to_upload: t_file[] = [];
    //console.log(my_files);
    for (const file_name of my_files) {
      //console.log(file_name);
      if (phase) {
        const striped_file_name = file_name.split(".");
        const index = Number.parseInt(striped_file_name[0], 10);
        if (index >= phase.first_token_id && index <= phase.last_token_id) {
          const file_path = path.join(folder_path, file_name);
          const content = await fs.promises.readFile(file_path);
          const type = mime.getType(file_path);
          //console.log(file_path, type);
          if (type) {
            const my_file = new File([content], file_name, { type });
            //const my_blob = new Blob([content]);
            to_upload.push(my_file);
          }
        }
      } else {
        const file_path = path.join(folder_path, file_name);
        const content = await fs.promises.readFile(file_path);
        const type = mime.getType(file_path);
        //console.log(file_path, type);
        if (type) {
          const my_file = new File([content], file_name, { type });
          //const my_blob = new Blob([content]);
          to_upload.push(my_file);
        }
      }
    }
    //console.log(to_upload);
    const storage = new NFTStorage({ token: api_token });
    cid = await storage.storeDirectory(to_upload);
    console.log({ cid });
    status = await storage.status(cid);
    console.log(status);
  } catch (error) {
    console.log(error);
  }
  return { cid, status };
};
