import fs from "fs";
import path from "path";
import {
  t_formats,
  DYNAMIC_ARTIFACT,
  t_metadata,
  base_metadata,
  STATIC_ARTIFACT,
  STATIC_THUMBNAIL,
} from "./config";

const create_unknown_metadata = async (
  metadata_folder_path: string,
  artifactUri: string,
  displayUri: string,
  thumbnailUri: string,
  token_id: number,
  level = 0
) => {
  try {
    const index = `${token_id}`.padStart(5, "0");
    const formats: t_formats[] = [
      {
        ...DYNAMIC_ARTIFACT,
        uri: artifactUri,
      },
      { ...STATIC_ARTIFACT, uri: displayUri },
      { ...STATIC_THUMBNAIL, uri: thumbnailUri },
    ];
    //const my_format: t_formats = { ...DYNAMIC_ARTIFACT };
    //const asset_uri = `ipfs://${artifactUri}`; //`ipfs://${fingerz_unrevealed_assets}/${index}.jpg`;
    //my_format.uri = asset_uri;
    const metadata: t_metadata = { ...base_metadata };
    metadata.id = token_id;
    metadata.name = `#${index}`;
    metadata.artifactUri = artifactUri;
    metadata.displayUri = displayUri;
    metadata.thumbnailUri = thumbnailUri;
    metadata.formats = formats;
    metadata.attributes = metadata.attributes.push({
      name: "level",
      value: `${level}`,
    });
    const metadata_file_path = path.join(
      metadata_folder_path,
      `${token_id}.level.${level}.json`
    );
    fs.promises.writeFile(metadata_file_path, JSON.stringify(metadata), "utf8");
  } catch (error) {
    console.log(error);
  }
};
export default create_unknown_metadata;
