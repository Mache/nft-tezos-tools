import path from "path";
import { load_collection } from "./load_collection";
import sha256_hash from "./sha256_hash";
const provenance_from_collection = async () => {
  let hashes = "";
  const collection_file_path = path.join(__dirname, "collection.json");
  const collection = await load_collection(collection_file_path);
  collection
    .entrySeq()
    .sort(([index_a, collectible_a], [index_b, collectible_b]) => {
      return index_a - index_b;
    })
    .forEach(([index, collectible]) => {
      const level_cero_hash = collectible.levels.get(0)?.artifact_hash;
      if (level_cero_hash) {
        hashes += level_cero_hash;
      } else {
        throw new Error("ARTIFACT HASH NOT FOUND");
      }
    });

  const my_buffer = Buffer.from(hashes);
  const provenance_hash = await sha256_hash(my_buffer);
  return provenance_hash;
};
/*const provenance_from_metadata = async () => {
  let hashes = "";
  try {
    const all_metadata_files = await fs.promises.readdir(metadata_folder_path);
    const to_end = all_metadata_files.length - 1;
    const from_id = 0;
    for (const metadata_file_name of all_metadata_files) {
      const metadata_file_name_splited = metadata_file_name.split(".");
      const metadata_id = Number.parseInt(metadata_file_name_splited[0], 10);
      if (metadata_id >= from_id && metadata_id <= to_end) {
        const metadata_file_path = path.join(
          metadata_folder_path,
          metadata_file_name
        );
        const metadata_level = Number.parseInt(
          metadata_file_name_splited[2],
          10
        );
        if (metadata_level == 0) {
          const metadata_buffer = await fs.promises.readFile(
            metadata_file_path
          );
          const metadata_stringify = metadata_buffer.toString();
          const metadata: t_metadata = JSON.parse(
            metadata_stringify
          ) as t_metadata;
          console.log(metadata.artifactHash);
          hashes += metadata.artifactHash;
        }
      }
    }
    console.log(hashes);
    const my_buffer = Buffer.from(hashes);
    const provenance_hash = await sha256_hash(my_buffer);
    console.log(provenance_hash);
  } catch (error) {
    console.log(error);
  }
};*/

provenance_from_collection().then((result) => console.log(result));
