import moment from "moment";
interface t_date {
  from_date: string;
  to_date: string;
}
const create_date = ({ from_date, to_date }: t_date) => {
  //console.log(moment.unix(1651150020).format());
  console.log("From date    : ", moment(from_date).unix());
  console.log("To date      : ", moment(to_date).unix());
};

create_date({
  from_date: "2022-07-30T21:00:00-03:00",
  to_date: "2022-08-01T21:00:00-03:00",
});

//4.147301
