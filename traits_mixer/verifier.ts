import fs from "fs";
import { assets_folder_path } from "./config";
const sha256_hash = async (my_buffer: Buffer) => {
  const { createHash } = await import("crypto");
  const sha526_hash = createHash("sha256");
  sha526_hash.update(my_buffer);
  return sha526_hash.digest("hex");
};

const verifier = async (
  image_file_path: string,
  image_index: string,
  indexed_hash_from_metadata: string,
  artifact_hash_from_metadata: string
) => {
  try {
    const artifact_buffer = await fs.promises.readFile(image_file_path);
    const artifact_hash = await sha256_hash(artifact_buffer);
    const indexed_hash = await sha256_hash(
      Buffer.from(`${image_index}@${artifact_hash}`)
    );
    const Integrity = artifact_hash_from_metadata === artifact_hash;
    const Indexed = indexed_hash_from_metadata === indexed_hash;
    console.log(`Integrity Verified: ${Integrity ? "yes" : "no"}`);
    console.log(`Index Verified: ${Indexed ? "yes" : "no"}`);
  } catch (error) {
    console.log(error);
  }
};

verifier(
  `${assets_folder_path}/85.level.0.png`,
  "85",
  "ec624cba1147477ee08145e7a6371f73f43b23453f45984126b28be635565f8f",
  "8e033323965bde288a13086cabccf4a343eb8e8b0ec18a9f1abe02133e4c1c3b"
);
