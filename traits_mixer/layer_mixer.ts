import path from "path";
import sharp from "sharp";

import fs from "fs";
import sha256_hash from "./sha256_hash";
import {
  assets_folder_path,
  width,
  height,
  t_overlay_options,
  t_attributes,
} from "./config";
import { List } from "immutable";

const layer_mixer = async (
  collection_index: number,
  layers: t_overlay_options,
  level = 0,
  attributes: t_attributes = List()
) => {
  const asset_file_path = path.join(
    assets_folder_path,
    `${collection_index}.level.${level}.png`
  );
  await sharp({
    create: {
      width,
      height,
      channels: 4,
      background: { r: 0, g: 0, b: 0 },
    },
  })
    .composite(layers.toArray())
    .toFile(asset_file_path);

  const my_sharp_buffer = await fs.promises.readFile(asset_file_path);
  const artifact_hash = await sha256_hash(my_sharp_buffer);
  const indexed_hash = await sha256_hash(
    Buffer.from(`${collection_index}@${artifact_hash}`)
  );
  return {
    artifact_hash,
    indexed_hash,
    attributes,
    collection_index,
    level,
  };
};
export default layer_mixer;
