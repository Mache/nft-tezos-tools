import fs from "fs";
import { Map } from "immutable";
import path from "path";
import { dot_extension, path_to_categories, rarity_separator } from "./config";

export interface t_category {
  category_name: string;
  layers: t_layers;
}
export interface t_layer {
  rarity: number;
  input: Buffer;
}
export type t_assets = Map<number, t_category>;
export type t_layers = Map<string, t_layer>;

const load_assets = async () => {
  const all_categories = await fs.promises.readdir(path_to_categories);
  let assets: t_assets = Map();
  for (const category of all_categories) {
    const category_index = Number.parseInt(category.slice(0, 2), 10);
    const category_name = category.slice(3);
    const path_to_layers = path.join(path_to_categories, category);
    const all_layers = await fs.promises.readdir(path_to_layers);
    let layers: t_layers = Map();
    for (const layer of all_layers) {
      const [layer_name, layer_rarity] = layer
        .slice(0, dot_extension.length * -1)
        .split(rarity_separator);
      const rarity = Number.parseInt(layer_rarity, 10);
      const path_to_layer = path.join(path_to_layers, layer);
      const layer_buffer = await fs.promises.readFile(path_to_layer);
      const my_layer: t_layer = { rarity, input: layer_buffer };
      layers = layers.set(layer_name, my_layer);
    }
    const my_category: t_category = { category_name, layers };
    assets = assets.set(category_index, my_category);
  }
  return assets;
};

export default load_assets;
