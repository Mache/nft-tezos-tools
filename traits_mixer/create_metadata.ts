import fs from "fs";
import { List } from "immutable";
import path from "path";
import {
  t_collectible,
  t_formats,
  STATIC_ARTIFACT,
  t_metadata,
  base_metadata,
  dot_extension,
  t_attribute,
} from "./config";

export const create_metadata = async (
  metadata_folder_path: string,
  ipfs_folder_hash: string,
  token_id: number,
  collectible: t_collectible,
  level = 0
) => {
  try {
    const index = `${token_id}`.padStart(5, "0");
    const my_format: t_formats = { ...STATIC_ARTIFACT };
    const asset_uri = `ipfs://${ipfs_folder_hash}/${collectible.id}.level.${level}${dot_extension}`; //`ipfs://${fingerz_unrevealed_assets}/${index}.jpg`;
    my_format.uri = asset_uri;
    const metadata: t_metadata = { ...base_metadata };
    metadata.id = token_id;
    metadata.name = `#${index}`;
    metadata.artifactUri = asset_uri;
    metadata.displayUri = asset_uri;
    metadata.thumbnailUri = asset_uri;
    metadata.formats = [my_format];
    const { attributes, artifact_hash, indexed_hash } = collectible.levels.get(
      level,
      {
        attributes: List<t_attribute>(),
        artifact_hash: "",
        indexed_hash: "",
      }
    );
    metadata.artifactHash = artifact_hash;
    metadata.indexedHash = indexed_hash;
    if (attributes.size > 0) {
      metadata.attributes = attributes.push({
        name: "level",
        value: `${level}`,
      });
    }

    //metadata.attributes.push({ name: "level", value: `${level}` });
    const metadata_file_path = path.join(
      metadata_folder_path,
      `${token_id}.level.${level}.json`
    );
    fs.promises.writeFile(metadata_file_path, JSON.stringify(metadata), "utf8");
  } catch (error) {
    console.log(error);
  }
};
