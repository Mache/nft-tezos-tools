import fs from "fs";

import path from "path";
import { dot_extension, path_to_categories, rarity_separator } from "./config";
export interface t_traits {
  [name: string]: number;
}
export interface t_category {
  totalWeight: number;
  traits: t_traits;
}

export type t_assets = { [category_name: string]: t_category };

const load_assets = async () => {
  const all_categories = await fs.promises.readdir(path_to_categories);
  let assets: t_assets = {};
  for (const category of all_categories) {
    const category_index = Number.parseInt(category.slice(0, 2), 10);
    const category_name = category.slice(3);
    const path_to_layers = path.join(path_to_categories, category);
    const all_layers = await fs.promises.readdir(path_to_layers);
    let layers: t_traits = {};
    let totalWeight = 0;
    for (const layer of all_layers) {
      const [layer_name, layer_rarity] = layer
        .slice(0, dot_extension.length * -1)
        .split(rarity_separator);
      const rarity = Number.parseInt(layer_rarity, 10);
      totalWeight += rarity;
      layers = { ...layers, [layer_name]: rarity };
    }
    const my_category: t_category = { totalWeight, traits: layers };
    assets = { ...assets, [category_name]: my_category };
  }
  return assets;
};

export default load_assets;

const readAssets = async () => {
  try {
    const assets = await load_assets();
    console.log(assets);
    fs.writeFileSync("rarity.json", JSON.stringify(assets));
  } catch (error) {
    console.log(error);
  }
};

readAssets();
