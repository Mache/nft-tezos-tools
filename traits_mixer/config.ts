import { List, Map } from "immutable";
import path from "path";
import sharp from "sharp";
export const resume = true;
export const collection_size = 9999;
export const path_to_categories = path.join(__dirname, "traits_categories");
export const width = 540;
export const height = width;
export const rarity_separator = "#";
export const max_duplication_factor = 10;
export const throw_on_max_duplication_factor = false;
export const assets_folder_name = "assets";
export const assets_folder_path = path.join(__dirname, assets_folder_name);
export const max_level = 7;
export const min_level = 0;
export const dot_extension = ".png";
export type t_attribute = { name: string; value: string };
export type t_attributes = List<t_attribute>;
export interface t_level {
  attributes: t_attributes;
  artifact_hash: string;
  indexed_hash: string;
}
export type t_levels = Map<number, t_level>;

export interface t_collectible {
  id: number;
  attributes: t_attributes;
  attributes_hash: string;
  same_as?: number[];
  levels: t_levels;
}

export type t_overlay_options = List<sharp.OverlayOptions>;
export type t_collection = Map<number, t_collectible>;
export interface t_phase {
  first_token_id: number;
  last_token_id: number;
}
export const unknown_metadata_folder_name = "unknown_metadata";
export const unknown_metadata_folder_path = path.join(
  __dirname,
  unknown_metadata_folder_name
);
export const metadata_folder_name = "metadata";
export const metadata_folder_path = path.join(__dirname, metadata_folder_name);
export const fa2_contract = "KT1D71degk4dMLtAWdpWgk8Ycg1tfeNdBNHz"; // Pool
export const reward_pool_wallet = "tz1ggkUZ9nQYgkXGQVAPFWUyvhbCHnfRrVNn";
export const sale_contract = "KT1W8Q1facYYeiwMLh1JVEGE9YZcsUXwXrk3";
export const mache_wallet = "tz1LgP5DCcgBZ6AaHQw5iwDynCi21G9MKXxy";
export const pogo_wallet = "tz1PP1gBnK7iFrF9747iHbCYsuJXnzmahQpJ";
export const luchota_wallet = "tz1gEEMESBfMQFrPLmGhkGaxSRe7Mxo1ousA";
export const fingerz_unrevealed_assets =
  "bafybeifrmmaqwzaqnhcl2bj63fe2mcite3hx6oqjve6hulbtejnwvl7khu";
export const hidden_token_artifact =
  "ipfs://bafybeifrmmaqwzaqnhcl2bj63fe2mcite3hx6oqjve6hulbtejnwvl7khu"; // 540X540 MP4
export const hidden_token_display =
  "ipfs://bafkreiezp4j2maxrpvwrojl5mfweqbanp72eorjrjh4j45ybdgv6clhhcy"; //540x540 PNG
export const hidden_token_thumbnail =
  "ipfs://bafkreifc5kk6npphuna24e7y5rybyhr7joohrlo6kplqz6bk7ki52lzace"; // 350x350 PNG
export const fingerz_revealed_assets =
  "bafybeigxhzkgsznkttgj5jbhpijta4o6efstxxocvscze7i4zrrdupr2ca";
export const creators = [pogo_wallet, luchota_wallet, mache_wallet];
export enum t_mimeType {
  PNG = "image/png",
  MP4 = "video/mp4",
}
export const t_dimension = {
  FULL: `${width}x${height}`,
  THUMBNAIL: `350x350`,
};
export enum t_unit {
  PX = "px",
}
export interface t_formats {
  uri: string;
  mimeType: t_mimeType;
  dimensions: {
    value: string;
    unit: t_unit;
  };
}
export interface t_metadata {
  id: number;
  name: string;
  description: string;
  decimals: number;
  isBooleanAmount: boolean;
  shouldPreferSymbol: boolean;
  creators: string[];
  artifactUri: string;
  displayUri: string;
  thumbnailUri: string;
  attributes: t_attributes;
  indexedHash: string;
  artifactHash: string;
  formats: t_formats[];
  royalties: {
    decimals: number;
    shares: {
      [key: string]: string;
    };
  };
}
export const STATIC_ARTIFACT = {
  uri: "",
  mimeType: t_mimeType.PNG,
  dimensions: {
    value: t_dimension.FULL,
    unit: t_unit.PX,
  },
};
export const STATIC_THUMBNAIL = {
  uri: "",
  mimeType: t_mimeType.PNG,
  dimensions: {
    value: t_dimension.THUMBNAIL,
    unit: t_unit.PX,
  },
};
export const DYNAMIC_ARTIFACT = {
  uri: "",
  mimeType: t_mimeType.MP4,
  dimensions: {
    value: t_dimension.FULL,
    unit: t_unit.PX,
  },
};
export const base_metadata = {
  id: 0,
  name: "",
  description:
    "10K Collection of Generative Profile Pictures - Fingerz is a project made with the power of friendship!",
  decimals: 0,
  isBooleanAmount: true,
  shouldPreferSymbol: false,
  creators,
  artifactUri: "",
  displayUri: "",
  thumbnailUri: "",
  attributes: List([
    { name: "back", value: "unknown" },
    { name: "body", value: "unknown" },
    { name: "planet", value: "unknown" },
    { name: "floor", value: "unknown" },
    { name: "eyes", value: "unknown" },
    { name: "mouth", value: "unknown" },
    { name: "head", value: "unknown" },
    { name: "chainz", value: "unknown" },
    { name: "tattz", value: "unknown" },
  ]),
  indexedHash: "",
  artifactHash: "",
  formats: [],
  royalties: {
    decimals: 2,
    shares: {
      [mache_wallet]: "8",
      [pogo_wallet]: "8",
      [luchota_wallet]: "4",
      [reward_pool_wallet]: "5",
    },
  },
};
