import { t_attributes, t_collection, t_levels } from "./config";
import fs from "fs";
import { Map, List } from "immutable";

interface stringify_collection {
  [key: string]: {
    id: number;
    attributes: t_attributes;
    attributes_hash: string;
    same_as?: number[];
    levels: {
      [key: string]: {
        attributes: { name: string; value: string }[];
        artifact_hash: string;
        indexed_hash: string;
      };
    };
  };
}

export const load_collection = async (
  collection_file_path: string
): Promise<t_collection> => {
  const collection_file = await fs.promises.readFile(collection_file_path);
  const my_collection: stringify_collection = JSON.parse(
    collection_file.toString()
  );

  let next_collection: t_collection = Map();
  Object.keys(my_collection).forEach((index) => {
    const my_collectible = my_collection[index];
    const my_levels = my_collectible.levels;
    const collection_index = Number.parseInt(index, 10);
    let next_levels: t_levels = Map();
    Object.keys(my_levels).forEach((level) => {
      const my_level = my_levels[level];
      const my_attributes: t_attributes = List(my_level.attributes);
      const level_index = Number.parseInt(level, 10);
      next_levels = next_levels.set(level_index, {
        ...my_level,
        attributes: my_attributes,
      });
    });
    next_collection = next_collection.set(collection_index, {
      ...my_collectible,
      levels: next_levels,
    });
  });
  return next_collection;
};
