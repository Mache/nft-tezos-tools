import { List } from "immutable";
import { t_layers } from "./load_assets";

interface t_weighted_layer {
  starting_weight: number;
  ending_weight: number;
  input: Buffer;
  layer_name: string;
}

const randomize_layers = (layers: t_layers) => {
  //FLATEN IN ONE DIMENSIONAL RARITY LIST
  let total_weight = 0;
  let weighted_layers: List<t_weighted_layer> = List();

  for (const layer_name of layers.keySeq()) {
    const my_layer = layers.get(layer_name);
    if (my_layer) {
      const { input, rarity } = my_layer;
      const starting_weight = total_weight + 1;
      const ending_weight = starting_weight + rarity;
      total_weight = ending_weight;
      weighted_layers = weighted_layers.push({
        starting_weight,
        ending_weight,
        input,
        layer_name,
      });
    }
  }

  // GET A RANDOM NUMBER UP TO MAX FLAT LENGTH
  const random_weight = Math.floor(Math.random() * (total_weight - 1)) + 1;
  // SEARCH THE LAYER CONTAINING THE RANDOM NUMBER
  const random_layer = weighted_layers.filter(
    (my_weighted_layer) =>
      random_weight <= my_weighted_layer.ending_weight &&
      random_weight >= my_weighted_layer.starting_weight
  );
  //console.log(random_weight);
  //console.log(weighted_layers.toJSON());

  if (random_layer.size > 0) {
    return random_layer.first();
  } else {
    throw new Error("TRAIT RARITY NOT FOUND IN ONE DIMENSIONAL LIST");
  }
  // EXPOSE RESULT
  //console.log(weighted_layers[index], my_rarity, next_rarity);
};
export default randomize_layers;
