import { dot_extension, rarity_separator } from "./config";

interface t_layer {
  from_rarity: number;
  to_rarity: number;
  file_name: string;
  layer_name: string;
}
export const strip_file_name = (layer_file_name: string) => {
  const layer_name_rarity = layer_file_name.slice(0, dot_extension.length * -1);
  const [layer_name, rarity] = layer_name_rarity.split(rarity_separator);
  return { layer_name, rarity };
};
const get_weight_randomized_layer = (layers_from_category: string[]) => {
  //FLATEN IN ONE DIMENSIONAL RARITY LIST
  let next_rarity = 0;
  const weighted_layers: t_layer[] = [];
  for (const file_name of layers_from_category) {
    const { rarity, layer_name } = strip_file_name(file_name);

    const to_rarity = next_rarity + Number.parseInt(rarity, 10);
    weighted_layers.push({
      from_rarity: next_rarity,
      to_rarity,
      file_name,
      layer_name,
    });
    next_rarity = to_rarity + 1;
  }
  // GET A RANDOM NUMBER UP TO MAX FLAT LENGTH
  const my_rarity = Math.floor(Math.random() * (next_rarity - 1));
  // SEARCH THE LAYER CONTAINING THE RANDOM NUMBER
  let index = 0;
  while (
    (weighted_layers[index] &&
      my_rarity < weighted_layers[index].from_rarity) ||
    (weighted_layers[index] && my_rarity > weighted_layers[index].to_rarity)
  ) {
    index++;
  }
  if (index > weighted_layers.length) {
    throw new Error("TRAIT RARITY NOT FOUND IN ONE DIMENSIONAL LIST");
  }
  // EXPOSE RESULT
  //console.log(weighted_layers[index], my_rarity, next_rarity);
  return weighted_layers[index];
};
export default get_weight_randomized_layer;
