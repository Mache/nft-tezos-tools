import { t_attributes } from "./config";

const attributes_md5_hash = async (attributes: t_attributes) => {
  /*const chain = attributes
    .toArray()
    .sort((a, b) => a.name.localeCompare(b.name))
    .map((att) => `${att.name}:${att.value}`)
    .join("-");
  //console.log(chain);
  return chain;*/
  const { createHash } = await import("crypto");
  const md5_hash = createHash("md5");
  md5_hash.update(
    attributes
      .toArray()
      .sort((a, b) => a.name.localeCompare(b.name))
      .map((att) => `${att.name}:${att.value}`)
      .join("-")
  );
  return md5_hash.digest("hex");
};
export default attributes_md5_hash;
