const sha256_hash = async (my_buffer: Buffer) => {
  const { createHash } = await import("crypto");
  const sha526_hash = createHash("sha256");
  sha526_hash.update(my_buffer);
  return sha526_hash.digest("hex");
};

export default sha256_hash;
